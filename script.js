// JavaScript code for typewriter effect and increasing dots
const text = "Vestige+ Reborn";
let index = 0;
let dotCount = 1; // Start with one dot
const maxDotCount = 3; // Maximum number of dots

function typeWriter() {
    document.getElementById("typewriter").innerText = text.slice(0, index);
    index++;

    if (index <= text.length) {
        setTimeout(typeWriter, 100); // Adjust the speed (in milliseconds) of the typewriter effect
    } else {
        // Start increasing dots after typewriter finishes
        increaseDots();
    }
}

function increaseDots() {
    const typewriterElement = document.getElementById("typewriter");

    setTimeout(() => {
        typewriterElement.innerText = text + ".".repeat(dotCount);

        dotCount++;
        if (dotCount > maxDotCount) {
            dotCount = 1; // Reset dot count after reaching the maximum
            typewriterElement.innerText = text + "."; // Reset dots to one after each iteration
        }

        increaseDots();
    }, 500); // Adjust the speed (in milliseconds) of increasing dots
}

// Start the typewriter effect when the page is loaded
window.onload = function () {
    typeWriter();
};